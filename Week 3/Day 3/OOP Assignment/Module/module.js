// Import class
const persegi = require('../BangunDatar/persegi.js')
const persegipanjang = require('../BangunDatar/persegipanjang.js')
const lingkaran = require('../BangunDatar/lingkaran.js')

const balok = require('../BangunRuang/balok.js')
const kubus = require('../BangunRuang/kubus.js')
const tabung = require('../BangunRuang/tabung.js')
const kerucut = require('../BangunRuang/kerucut.js')


/* Start class Module */
class Module {
  constructor() {
    this.name = 'Menghitung Bangun Datar dan Bangun Ruang'
  }

  /* Ini bangun datar */
  // function luas Persegi
  menghitungLuasPersegi(sisi) {
    let hitungLuasPersegi = new persegi(sisi)
    return hitungLuasPersegi.menghitungLuas()
  }

  // function keliling Persegi
  menghitungKelilingPersegi(sisi) {
    let hitungKelilingPersegi = new persegi(sisi)
    return hitungKelilingPersegi.menghitungKeliling()
  }

  // function luas Persegi Panjang
  menghitungLuasPersegiPanjang(panjang, lebar) {
    let hitungLuasPersegiPanjang = new persegipanjang(panjang, lebar)
    return hitungLuasPersegiPanjang.menghitungLuas()
  }

  // function keliling Persegi Panjang
  menghitungKelilingPersegiPanjang(panjang, lebar) {
    let hitungKelilingPerseguPanjang = new persegipanjang(panjang, lebar)
    return hitungKelilingPerseguPanjang.menghitungKeliling()
  }

  // function luas Lingkaran
  menghitungLuasLingkaran(radius) {
    let hitungLuasLingkaran = new lingkaran(radius)
    return hitungLuasLingkaran.menghitungLuas()
  }

  // function keliling Lingkaran
  menghitungKelilingLingkaran(radius) {
    let hitungKelilingLingkaran = new lingkaran(radius)
    return hitungKelilingLingkaran.menghitungKeliling()
  }

  /* Ini end bangun datar */

  /* Start bangun ruang */

  // function luas Balok
  menghitungLuasBalok(panjang,lebar,tinggi)  {
    let hitungLuasBalok = new balok(panjang,lebar,tinggi)
    return hitungLuasBalok.menghitungLuas()
  }

  // function Volume Balok
  menghitungVolumeBalok(panjang,lebar,tinggi)  {
    let hitungVolumeBalok = new balok(panjang,lebar,tinggi)
    return hitungVolumeBalok.menghitungVolume()
  }

  // function luas Kubus
  menghitungLuasKubus(sisi)  {
    let hitungLuasKubus = new kubus(sisi)
    return hitungLuasKubus.menghitungLuas()
  }

  // function Volume Kubus
  menghitungVolumeKubus(sisi)  {
    let hitungVolumeKubus = new kubus(sisi)
    return hitungVolumeKubus.menghitungVolume()
  }

  // function luas Tabung
  menghitungLuasTabung(radius,tinggi)  {
    let hitungLuasTabung = new tabung(radius,tinggi)
    return hitungLuasTabung.menghitungLuas()
  }

  // function Volume Tabung
  menghitungVolumeTabung(radius,tinggi)  {
    let hitungVolumeTabung = new tabung(radius,tinggi)
    return hitungVolumeTabung.menghitungVolume()
  }

  // function luas Kerucut
  menghitungLuasKerucut(radius,tinggi)  {
    let hitungLuasKerucut = new kerucut(radius,tinggi)
    return hitungLuasKerucut.menghitungLuas()
  }

  // function Volume Kerucut
  menghitungVolumeKerucut(radius,tinggi)  {
    let hitungVolumeKerucut = new kerucut(radius,tinggi)
    return hitungVolumeKerucut.menghitungVolume()
  }

  /* End bangun ruang */

}
/* End class Module */

module.exports = Module
