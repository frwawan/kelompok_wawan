// Import module.js
const importModule = require('./module.js')

// make object of importModule
const hitung = new importModule()

/* Start make calculate persegi */
let luasPersegi = hitung.menghitungLuasPersegi(20);
console.log(luasPersegi);

let kelilingPersegi = hitung.menghitungKelilingPersegi(30)
console.log(kelilingPersegi);
/* End make calculate persegi */

/* Start make calculate persegi panjang */
let luasPersegiPanjang = hitung.menghitungLuasPersegiPanjang(10, 20)
console.log(luasPersegiPanjang);

let kelilingPersegiPanjang = hitung.menghitungKelilingPersegiPanjang(20, 30)
console.log(kelilingPersegiPanjang);
/* End make calculate persegi panjang */

/* start make calculate lingkaran */
let luasLingkaran = hitung.menghitungLuasLingkaran(7)
console.log(luasLingkaran);

let kelilingLingkaran = hitung.menghitungKelilingLingkaran(14)
console.log(kelilingLingkaran);
/* End make calculate lingkaran */

/* start make calculate Balok */
let luasBalok = hitung.menghitungLuasBalok(10,5,4)
console.log(luasBalok);

let volumeBalok = hitung.menghitungVolumeBalok(10,5,4)
console.log(volumeBalok);
/* End make calculate Balok */

/* start make calculate Kubus */
let luasKubus = hitung.menghitungLuasKubus(4)
console.log(luasKubus);

let volumeKubus = hitung.menghitungVolumeKubus(4)
console.log(volumeKubus);
/* End make calculate Kubus */

/* start make calculate Tabung */
let luasTabung = hitung.menghitungLuasTabung(4,10)
console.log(luasTabung);

let volumeTabung = hitung.menghitungVolumeTabung(4,10)
console.log(volumeTabung);
/* End make calculate Tabung */

/* start make calculate Kerucut */
let luasKerucut = hitung.menghitungLuasKerucut(4,10)
console.log(luasKerucut);

let volumeKerucut = hitung.menghitungVolumeKerucut(4,10)
console.log(volumeKerucut);
/* End make calculate Kerucut */
