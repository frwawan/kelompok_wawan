const bangunruang = require('./bangunruang.js')

class Kubus extends bangunruang {

    constructor(sisi) {
    super('Kubus')
    this.sisi = sisi
    }

    menghitungVolume() {
    super.menghitungVolume()
    return this.sisi * this.sisi * this.sisi
    }

     menghitungLuas() {
    return this.sisi * this.sisi * 6
    }
}

module.exports = Kubus

/*const kubus1 = new Kubus(4);
console.log(kubus1);
console.log(kubus1.menghitungLuas());
console.log(kubus1.menghitungVolume());*/
