const bangunruang = require ('./bangunruang.js')


class Kerucut extends bangunruang {

    // Make constructor with radius and tinggi of Kerucut
    constructor(radius,tinggi) {
      super('Kerucut') // call the BangunRuang constructor
      this.radius = radius // Instance variable
      this.tinggi = tinggi // Instance variable
    }

    #menghitungGarisPelukis() {
      return Math.sqrt((this.radius ** 2 + this.tinggi**2));
    }

    // Overriding menghitungLuas from BangunRuang class
    menghitungLuas() {
      return (Math.PI * this.radius ** 2) + ( Math.PI * this.radius * this.#menghitungGarisPelukis())
    }

    // Overriding menghitungVolume from BangunRuang class
    menghitungVolume() {
      return 1/3 * Math.PI * this.radius ** 2 * this.tinggi
    }
  }


module.exports = Kerucut
