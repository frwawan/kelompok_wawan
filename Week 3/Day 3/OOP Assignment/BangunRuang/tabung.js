// Import BangunRuang class
const bangunruang = require('./bangunruang.js')

// This class is child or BangunRuang (Inheritance)
class Tabung extends bangunruang {

  // Make constructor with radius and tinggi of Tabung
  constructor(radius,tinggi) {
    super('Tabung') // call the BangunRuang constructor
    this.radius = radius // Instance variable
    this.tinggi = tinggi // Instance variable
  }

  // Overriding menghitungLuas from BangunRuang class
  menghitungLuas() {
    return (2*Math.PI * Math.pow(this.radius, 2)) + (2 * Math.PI * this.radius * this.tinggi)
  }

  // Overriding menghitungVolume from BangunRuang class
  menghitungVolume() {
    return Math.PI * (this.radius ** 2) * this.tinggi
  }
}

module.exports = Tabung

/* Test file running properly */
// const tabung1 = new Tabung(4,10);
// console.log(tabung1);
// console.log(tabung1.menghitungLuas());
// console.log(tabung1.menghitungVolume());
