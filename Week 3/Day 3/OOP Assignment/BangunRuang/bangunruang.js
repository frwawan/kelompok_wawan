// Import Bangun class
const bangun = require('../bangun.js');

/* Make BangunRuang Abstract Class */
class BangunRuang extends bangun {

  // Make constructor with name variable/property
  constructor(name) {
    super(name)

    if (this.constructor === BangunRuang) {
      throw new Error('This is abstract!')
    }
  }

  // menghitungLuas instance method
  menghitungLuas() {
    console.log('Luas bangun ruang');
  }

  // menghitungVolume instance method
  menghitungVolume() {
    console.log("Volume bangun ruang");
  }
}
/* End BangunDatar Abstract Class */

module.exports = BangunRuang

/* Test file running properly */
// const test = new BangunRuang('test');
// console.log(test); // Will retun Error of Abstract Class
