const connection = require ('../models/connection.js')


class TransaksiController{

  async getAll(req,res){
    try{
    var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id ORDER by t.id"
      connection.query(sql, function (err,result){
        if (err) throw err;
        res.json({
          status:"success",
          data:result
        })
      });
    }
    catch(e){
      res.json({
        status:"Error"
      })
    }
  }



  async getOne(req,res){
    try{
       var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?"
      connection.query(sql,[req.params.id], function (err,result){
        if (err) throw err;
        res.json({
          status:"success",
          data:result[0]
        })
      });
    }
    catch(e){
      res.json({
        status:"Error"
      })
      }
    }

    async create(req, res) {

    try {
      var sql='SELECT harga FROM barang WHERE id=?'
      connection.query(sql,[req.body.id_barang],function(err,result){
        if (err){
          res.json({
            status:"Error",
            error:err
          })
        }

      var total=result[0].harga*req.body.jumlah



      var sqlinsert='INSERT INTO transaksi(id_barang, id_pelanggan, jumlah, total) VALUES (?, ?, ?, ?)'

      connection.query(sqlinsert,[req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total],
        (e, result) => {
          if (e){ // If error

          // If success it will return JSON of result
          res.json({
            status: 'Error',
            error: e
          })
        }


    var sqlselect = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?"
   connection.query(sqlselect,[result.insertId], function (err,result){
     if (err){
     res.json({
       status:"Error",
       error:err
     })
   }
   res.json({
     status:"success adding data",
     data:result[0]
 })

});
}
)
});
}   catch (err) {
    // If error will be send Error JSON
    res.json({
      status: "Error",
      error:err
    })
   }

  }




  async update(req, res) {
  try {
    var sql='SELECT harga FROM barang WHERE id=?'
    connection.query(sql,[req.body.id_barang],function(err,result){
      if (err){
        res.json({
          status:"Error",
          error:err
        })
      }

    var total=result[0].harga*req.body.jumlah


    var sqlupdate = 'UPDATE transaksi t SET id_barang = ?, id_pelanggan = ?, jumlah = ?, total = ? WHERE id = ?'

    connection.query(
      sqlupdate,
      [req.body.id_barang, req.body.id_pelanggan, req.body.jumlah, total, req.params.id],
      (err, result) => {
        if (err) {
          res.json({
            status: "Error",
            error: err
          });
        } // If error



        var sqlselect = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?"
       connection.query(sqlselect,[req.params.id], function (err,result){
         if (err){
         res.json({
           status:"Error",
           error:err
         })
       }
       res.json({
         status:"success adding data",
         data:result

     })
      //  console.log(result[0])
    })
        // If success it will return JSON of result
        // res.json({
        //   status: 'Success',
        //   data: result
        // })
        console.log(result)
      }
    )
  })
} catch (err) {
    // If error will be send Error JSON
    res.json({
      status: "Error",
      error: err
    })
  }
}


async delete(req, res) {
    try {

      var sql = 'DELETE FROM transaksi t WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }

}



module.exports=new TransaksiController
