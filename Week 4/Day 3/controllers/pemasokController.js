const connection = require('../models/connection.js')

class PemasokController {

  // Read All
  async getAll(req, res) {
    try {
      var sql = "SELECT * FROM pemasok"
      connection.query(sql, (err, result) => {
        if (err) {
          res.json({
            status:'Error',
            error:err
          })
        }
        res.json({
          status: "Success",
          data: result
        })
      });
    } catch (e) {
      res.json({
        status: "Error",
        error:e
      })
    }
  }


  // Read One
  async getOne(req,res) {
    try {
      var sql = "SELECT * FROM pemasok pe WHERE pe.id = ?";
      connection.query(sql, [req.params.id], (err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        res.json({
          status:'Success',
          data:result[0]
        })
      })
    } catch (e) {
      res.json({
        status:'Error',
        error:e
      })
    }
  }

  // Create
  async create(req,res) {
    try {
      var sql = "INSERT INTO pemasok(nama) VALUES (?)"
      connection.query(sql,[req.body.nama],(err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        var sqlresult = "SELECT * FROM pemasok pe WHERE pe.id = ?";
        connection.query(sqlresult, [result.insertId], (err,result) => {
          if (err) {
            res.json({
              status:"Error",
              error:err
            })
          }
          res.json({
            status:'Success',
            data:result[0]
          })
        })
      })
    } catch (e) {
      res.json({
        status:"Error",
        error:e
      })
    }
  }

  // Update
  async update(req,res) {
    try {
      var sql = "UPDATE pemasok SET nama = ? WHERE id = ?"
      connection.query(sql,[req.body.nama,req.params.id], (err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        var sqlresult = "SELECT * FROM pemasok pe WHERE pe.id = ?";
        connection.query(sqlresult, [req.params.id], (err,result) => {
          if (err) {
            res.json({
              status:"Error",
              error:err
            })
          }
          res.json({
            status:'Success',
            data:result[0]
          })
        })
      })
    } catch (e) {
      res.json({
        status:"Error",
        error:e
      })
    }
  }

  // Delete
  async delete(req,res) {
    try {
      var sql = "DELETE FROM pemasok WHERE id = ?"
      connection.query(sql,[req.params.id], (err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        var sqlresult = "SELECT * FROM pemasok"
        connection.query(sqlresult, (err, result) => {
          if (err) {
            res.json({
              status:'Error',
              error:err
            })
          }
          res.json({
            status: "Success",
            data: result
          })
        });
      })
    } catch (e) {
      res.json({
        status:"Error",
        error:e
      })
    }
  }



}

module.exports = new PemasokController;
