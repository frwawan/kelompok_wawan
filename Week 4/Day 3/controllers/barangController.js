const connection = require('../models/connection.js')

class BarangController {

  // Read All
  async getAll(req, res) {
    try {
      var sql = "SELECT * FROM barang"
      connection.query(sql, (err, result) => {
        if (err) {
          res.json({
            status:'Error',
            error:err
          })
        }
        res.json({
          status: "Success",
          data: result
        })
      });
    } catch (e) {
      res.json({
        status: "Error",
        error:e
      })
    }
  }


  // Read One
  async getOne(req,res) {
    try {
      var sql = "SELECT * FROM barang b WHERE b.id = ?";
      connection.query(sql, [req.params.id], (err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        res.json({
          status:'Success',
          data:result[0]
        })
      })
    } catch (e) {
      res.json({
        status:'Error',
        error:e
      })
    }
  }

  // Create
  async create(req,res) {
    try {
      var sql = "INSERT INTO barang(nama,harga,id_pemasok) VALUES (?, ?, ?)"
      connection.query(sql,[req.body.nama, req.body.harga, req.body.id_pemasok],(err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        var sqlresult = "SELECT * FROM barang b WHERE b.id = ?";
        connection.query(sqlresult, [result.insertId], (err,result) => {
          if (err){
            res.json({
              status:"Error",
              error:err
            })
          }
          res.json({
           status:'Success',
           data:result[0]
          })
        })
      })
    } catch (e) {
      res.json({
        status:"Error",
        error:e
      })
    }
  }

  // Update
  async update(req,res) {
    try {
      var sql = "UPDATE barang SET nama = ?,harga = ?,id_pemasok = ? WHERE id = ?"
      connection.query(sql,[req.body.nama, req.body.harga, req.body.id_pemasok, req.params.id], (err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        var sqlresult = "SELECT * FROM barang b WHERE b.id = ?";
        connection.query(sqlresult, [req.params.id], (err,result) => {
          if(err) {
            res.json({
              status:"Error",
              error:err
            })
          }
          res.json({
            status:'Success',
            data:result[0]
          })
        })
      })
    } catch (e) {
      res.json({
        status:"Error",
        error:e
      })
    }
  }

  // Delete
  async delete(req,res) {
    try {
      var sql = "DELETE FROM barang WHERE id = ?"
      connection.query(sql,[req.params.id], (err,result) => {
        if (err) {
          res.json({
            status:"Error",
            error:err
          })
        }
        res.json({
          status:'Success',
          data:result
        })
      })
    } catch (e) {
      res.json({
        status:"Error",
        error:e
      })
    }
  }



}

module.exports = new BarangController;
