// Import Modules
const express =require('express')
const app = express();

// Import Routes Modules
const barangRoutes = require('./routes/barangRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')
const transaksiRoutes=require('./routes/transaksiRouter.js')

// const transaksiRoutes = require('./routes/transaksiRoutes.js')


app.use(express.urlencoded({extended:false}));

app.use('/barang', barangRoutes)
app.use('/pelanggan', pelangganRoutes)
app.use('/pemasok', pemasokRoutes)
app.use('/transaksi', transaksiRoutes)
// app.use('/transaksi', transaksiRoutes)

app.listen(3000);
