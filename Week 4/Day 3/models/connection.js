// Import mysql
const mysql = require('mysql');

// Make mysql connection
const connection = mysql.createConnection({
  host:'localhost',
  user:'username',
  password:'password',
  database:'penjualan'
})

// Check createConnection
// connection.connect(function(err) {
//   if (err) throw err;
//   console.log("Connected!");
// });

// Export module;
module.exports = connection;
