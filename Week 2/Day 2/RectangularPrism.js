const index = require('./menu_index.js')

function volRectangularPrism(length,width,height) {
  return length*width*height ;
}

function inputLength() {
  index.rl.question("Length: ", length => {
    if (!isNaN(length) && !index.isEmptyOrSpaces(length)) {
      inputWidth(length);
    } else {
      console.log("Length must be number");
      inputLength();
    }
  })
}

function inputWidth(length) {
  index.rl.question("Width: ", width => {
    if (!isNaN(width) && !index.isEmptyOrSpaces(width)) {
      inputHeight_2(length, width)
    } else {
      console.log("Width must be number");
      inputWidth(length);
    }
  })
}

function inputHeight_2(length, width) {
  index.rl.question("Height: ", height => {
    if (!isNaN(height) && !index.isEmptyOrSpaces(height)) {
      console.log(`Rectangular Prism Volume: ${volRectangularPrism(length, width, height)}`)
      index.inputOption()
    } else {
      console.log("Height must be number");
      inputHeight_2(length, width);
    }
  })
}

module.exports.inputLength = inputLength
