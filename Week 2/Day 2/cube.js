const index = require('./menu_index.js')

function volCube(length){
    return length**3
}


function inputLength(){
  index.rl.question(`\nLength: `, length => {
        if(!isNaN(length) && !index.isEmptyOrSpaces(length)){
            console.log(`\nVolume cube is ${volCube(length)}`)
            index.inputOption()
        }else{
            console.log(`length must be number`)
            inputLength()
        }
    })
}


module.exports.inputLength = inputLength
